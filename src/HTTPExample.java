import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.net.URL;
import java.net.HttpURLConnection;

public class HTTPExample {

    public static void main(String [] args) throws IOException {
        // variables
        Scanner input = new Scanner(System.in);
        String fname = null;
        String lname = null;
        String email = null;
        String studentID = null;
        // User input
        System.out.print("Enter first name: ");
        fname = input.next();
        System.out.print("Enter last name: ");
        lname = input.next();
        System.out.print("Enter your email: ");
        email = input.next();
        System.out.print("Enter student ID: ");
        studentID = input.next();

        try {
            String urlLink = "http://localhost/java/example.php?fname="+fname + "&lname=" +lname + "&email=" +email + "&studentID=" +studentID;
            URL url = new URL(urlLink);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while((line = bufferedReader.readLine())!=null){
                stringBuilder.append(line);
                System.out.println(stringBuilder.toString());
            }
        }catch (Exception e){
                System.err.println(e.toString());
            }
        }
}


